CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
Simple as possible image cache effect, that cat process local images using
TinyPNG service keeping original files untouched.

REQUIREMENTS
------------
This module requires the following modules:
  * Image

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:

   - Administer TinyPNG (Tinypng Image Action module)

 * Get you API key from https://tinypng.com/developers.

 * Visit Administration » Configuration » Media » TinyPNG Settings:

   - Provide you API key and press "Save configuration".

 * Edit image style in Administration » Configuration » Media » Image styles:

   - Add "TinyPNG optimization" action and press "Update style".

MAINTAINERS
-----------
Current maintainers:
 * Andrey Khromyshev (profak) - https://drupal.org/u/profak

This project has been sponsored by:
 * Acronis International GmbH
   Acronis sets the standard for New Generation Data Protection through its
   backup, disaster recovery, and secure access solutions.
   Powered by the AnyData Engine and set apart by its image technology,
   Acronis delivers easy, complete and safe backups of all files,
   applications and OS across any environment—virtual, physical,
   cloud and mobile.

   Founded in 2003, Acronis protects the data of over 5 million consumers
   and 500,000 businesses in over 130 countries.
   With its more than 50 patents, Acronis’ products have been named
   best product of the year by Network Computing,
   TechTarget and IT Professional and cover a range of features,
   including migration, cloning and replication.

   For additional information, please visit www.acronis.com.
   Follow Acronis on Twitter: http://twitter.com/acronis.
